<?php

namespace Drupal\search_api_string_sanitization\Plugin\search_api\processor;

use Drupal\search_api\LoggerTrait;
use Drupal\search_api\Processor\FieldsProcessorPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Eliminates non-UTF-8 characters from the strings.
 *
 * @SearchApiProcessor(
 *   id = "search_api_text_sanitizer",
 *   label = @Translation("Search API String Sanitizer"),
 *   description = @Translation("Makes sure that strings have only valid UTF-8 characters."),
 *   stages = {
 *     "pre_index_save" = 0,
 *     "preprocess_index" = -20,
 *     "preprocess_query" = -20
 *   }
 * )
 */
class TextSanitizer extends FieldsProcessorPluginBase {

  use LoggerTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var static $processor */
    $processor = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $processor->setLogger($container->get('logger.factory')->get('search_api_string_sanitization'));

    return $processor;
  }

  /**
   * {@inheritdoc}
   */
  protected function process(&$value) {
    // We don't touch integers, NULL values or the like.
    if (is_string($value)) {
      $original = $value;

      // Ensure the string is valid UTF8.
      // Unknown characters will be replaced by a question mark.
      $value = mb_convert_encoding($value, 'UTF-8', 'UTF-8');

      // Special handling for an edge case. Example:
      // Invalid UTF-8 character 0xfffe at char #557
      // error messages in the log and Solr could not index all the nodes.
      $value = preg_replace('/[\x{fffe}-\x{ffff}]/u', '', $value);

      if ($value !== $original) {
        $this->logger->error('The text was altered during indexing: ' . $value);
      }
    }
  }

}

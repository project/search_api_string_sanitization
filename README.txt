# Search API String Sanitization

## Usage

After enabling the module, you need to add the provided processor (Search API String Sanitizer) for every Search API index where you'd like to use it.
